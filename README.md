# PaChrome

PaChrome, portmanteau of Pacman and Chrome. This is an user space oom killer for Google Chrome.

PaChrome make use of cgroup to track memory usage of Chrome, and kill Chrome as soon as it hits the memory limit.

Although the same thing can also be achieved simply by using cgroup, but there're some disadvantages:

* Kernel oom killer might kill the wrong process. If the main process is killed, the whole browser is gone, not just a single tab.
* Kernel will try its best to reclaim memory before killing. This means when Chrome hits memory limit, it will be unresponsive for a looooong time.

PaChrome solve the above problems by:

* Looking at the command line arguments, and only kill the renderer processes.
* Start killing as soon as the reclaim starts

## Requirements

### Build requirements

* libev

### Runtime requirements

* Memory cgroup support in the kernel
