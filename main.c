#define _GNU_SOURCE
#include <errno.h>
#include <linux/cn_proc.h>
#include <linux/connector.h>
#include <linux/netlink.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/eventfd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <regex.h>
#include <ev.h>
#include <dirent.h>
#include <limits.h>
#include <time.h>
#include <sys/inotify.h>
static const char *cgroot = NULL, *procpath;

static char pathbuf[PATH_MAX+2], pathbuf2[PATH_MAX+2];
static char *lbuf = NULL;
static size_t lbufsz = 0;

static int cgfd = -1;

static const char *rand_string(void) {
	int fd = open("/dev/urandom", O_RDONLY);
	char buf[8];
	read(fd, buf, sizeof(buf));

	const char *hex = "0123456789abcdef";
	static char ret[17];
	for (int i = 0; i < 8; i++) {
		ret[i*2] = hex[(buf[i]&0xf)];
		ret[i*2+1] = hex[(buf[i]>>8)&0xf];
	}
	close(fd);
	return ret;
}

static bool should_kill(int pid) {
	// Please don't kill the whole browser
	// read cmdline, don't kill processes with
	// --type=zygote or without any --type=
	int len = snprintf(pathbuf, PATH_MAX, "/proc/%d/cmdline", pid);
	if (len >= PATH_MAX)
		return false;

	int fd = open(pathbuf, O_RDONLY);
	len = read(fd, pathbuf2, sizeof(pathbuf2));
	close(fd);

	char *pos = pathbuf2;
	bool ret = false;
	while(pos < pathbuf2+len) {
		if (!strstr(pos, "--type")) {
			pos += strlen(pos)+1;
			continue;
		}
		ret = true;
		if (strstr(pos, "--type=zygote"))
			return false;
		if (strstr(pos, "--type=gpu-"))
			return false;
		pos += strlen(pos)+1;
	}
	return ret;
}

// Grace period. Memory pressure can be transient. So only
// start killing if the pressure doesn't go away after one
// grace period.
#define GRACE_PERIOD 5
#define GRACE_PERIOD_WAIT 2
static struct timespec last_p;
static uint32_t pressure_count = 0;
static void pressure_cb(EV_P_ ev_io *w, int revents) {
	(void)EV_A;
	(void)revents;
	uint64_t r;
	read(w->fd, &r, sizeof(r));

	struct timespec curr_p;
	int tret = clock_gettime(CLOCK_MONOTONIC_COARSE, &curr_p);
	time_t tdiff = curr_p.tv_sec-last_p.tv_sec;

	if (tret >= 0 && tdiff < GRACE_PERIOD) {
		pressure_count += r;
		return;
	}

	fprintf(stderr, "New grace period started, %u events in last period\n", pressure_count);
	last_p = curr_p;
	pressure_count = r;
	if (tret >= 0 && tdiff > GRACE_PERIOD+GRACE_PERIOD_WAIT) {
		fprintf(stderr, "Previous grace period has ended, don't kill\n");
		return;;
	}

	FILE *cpf = fopen(procpath, "r");
	char *line = NULL;
	size_t linesz = 0, max_mem = 0;
	pid_t tokill = 0;
	while (getline(&line, &linesz, cpf)) {
		pid_t pid = atol(line);

		if (should_kill(pid)) {
			snprintf(pathbuf, PATH_MAX, "/proc/%d/stat", pid);
			FILE *mf = fopen(pathbuf, "r");
			if (mf) {
				getline(&line, &linesz, mf);
				char *s = strrchr(line, ')');
				for (int i = 0; i < 22; i++)
					s = strchr(s, ' ')+1;

				size_t rss;
				sscanf(s, "%lu", &rss);

				if (max_mem < rss) {
					max_mem = rss;
					tokill = pid;
				}
			}
			fclose(mf);
		}
	}

	if (tokill) {
		fprintf(stderr, "Killing %u, memory usage %f\n", tokill, max_mem*4/1024.0f);
		snprintf(pathbuf, PATH_MAX, "/proc/%d/cmdline", tokill);

		int tmpfd = open(pathbuf, O_RDONLY);
		int len = read(tmpfd, pathbuf2, sizeof(pathbuf2));
		close(tmpfd);
		if (len > 0) {
			for (int i = 0; i < len; i++)
				if (pathbuf2[i] == '\0')
					pathbuf2[i] = ' ';
			pathbuf2[len] = '\0';
			fprintf(stderr, "cmdline: %s\n", pathbuf2);
		} else
			fprintf(stderr, "Failed to read cmdline\n");
		kill(tokill, SIGKILL);
	}
}


static inline int memory_pressure_efd(void) {
	char *buf;
	asprintf(&buf, "%s/%s", cgroot, "memory.pressure_level");

	int fd = open(buf, O_RDONLY);
	if (fd < 0)
		return -1;

	free(buf);

	int efd = eventfd(0, 0);
	asprintf(&buf, "%s/%s", cgroot, "cgroup.event_control");
	int cfd = open(buf, O_RDWR);
	if (cfd < 0)
		return -1;
	free(buf);

	int len = asprintf(&buf, "%d %d critical", efd, fd);
	write(cfd, buf, len);
	free(buf);

	return efd;
}

static void chldhandler(EV_P_ ev_signal *w, int revents) {
	(void)EV_A;
	(void)w;
	(void)revents;
	fprintf(stderr, "get signal chld %d\n", revents);
	int wstatus;
	waitpid(-1, &wstatus, 0);
	ev_break(EV_A_ EVBREAK_ALL);
}

static void sighandler(EV_P_ ev_signal *w, int revents) {
	(void)EV_A;
	(void)w;
	(void)revents;
	fprintf(stderr, "get signal int %d\n", revents);
	ev_break(EV_A_ EVBREAK_ALL);
}

static int clear_child(const char *path) {
	DIR *d = opendir(path);
	int dirfd = open(path, O_RDONLY|O_DIRECTORY);
	int retval = -1;
	if (!d || dirfd < 0)
		return -1;

	int ofd = openat(dirfd, "cgroup.procs", O_RDWR);
	if (ofd < 0)
		goto end;
	FILE *of = fdopen(ofd, "w");
	if (!of) {
		close(ofd);
		goto end;
	}

	struct dirent *dent;
	struct stat buf;
	while((dent = readdir(d))) {
		if (strncmp(dent->d_name, ".", 1) == 0 || strncmp(dent->d_name, "..", 2) == 0)
			continue;
		int ret = fstatat(dirfd, dent->d_name, &buf, 0);
		if (ret < 0)
			continue;
		if (!S_ISDIR(buf.st_mode))
			continue;
		char *buf = NULL;
		asprintf(&buf, "%s/%s", path, dent->d_name);
		if (!buf)
			continue;

		ret = clear_child(buf);
		free(buf);
		if (ret < 0)
			goto end2;

		asprintf(&buf, "%s/%s/cgroup.procs", path, dent->d_name);
		FILE *f = fopen(buf, "r");
		if (!f)
			goto end2;
		free(buf);

		uint64_t pid;
		while(fscanf(f, "%lu", &pid) != EOF) {
			fprintf(of, "%lu", pid);
			fflush(of);
		}
		fclose(f);

		// Everything should have been remove from child cgroup
		ret = unlinkat(dirfd, dent->d_name, AT_REMOVEDIR);
		if (ret < 0)
			goto end2;
	}

	retval = 0;
end2:
	fclose(of);
end:
	closedir(d);
	close(dirfd);
	return retval;
}

char *rstrip(char *str) {
	if (!str)
		return NULL;

	int p = strlen(str);
	while(str[p-1] == '\n' || str[p-1] == '\r')
		str[--p] = '\0';
	return str;
}

int wait_for_cgroup_release(void) {
	snprintf(pathbuf, PATH_MAX, "%s/cgroup.events", cgroot);
	int ifd = inotify_init();
	if (ifd < 0)
		return ifd;

	int ret = inotify_add_watch(ifd, pathbuf, IN_MODIFY);
	if (ret < 0)
		return ret;

	char ibuf[sizeof(struct inotify_event)+NAME_MAX+1];
	while(true) {
		ret = read(ifd, &ibuf, sizeof(ibuf));
		int efd = open(pathbuf, O_RDONLY);
		char buf[50];
		ret = read(efd, buf, sizeof(buf));
		if (strncmp(buf, "populated 0", 11) == 0)
			return 0;
	}
	return ret;
}

int main(int argc, char * const *argv) {
	if (argc < 3) {
		printf("Usage: %s <limit> <cmd>\n\n", argv[0]);
		printf("\tlimit         : Maximum memory allowed, in bytes (can be have\n");
		printf("\t                suffice like K, M, G\n");
		printf("Note: All child cgroups in the specified cgroup will be cleared\n");
		exit(1);
	}

	FILE *f = fopen("/proc/self/mounts", "r");
	if (!f)
		return EXIT_FAILURE;
	ssize_t ret;

	char *cgroot1 = NULL;
	while((ret = getline(&lbuf, &lbufsz, f)) >= 0) {
		char *path = strchr(lbuf, ' ')+1;
		char *type = strchr(path, ' ');
		*(type++) = '\0';
		char *last = strchr(type, ' ');
		*last = '\0';
		printf("%s %s\n", type, path);
		if (strncmp(type, "cgroup2 ", 7))
			continue;

		cgroot1 = path;
		break;
	}

	fclose(f);

	if (!cgroot1)
		return EXIT_FAILURE;

	f = fopen("/proc/self/cgroup", "r");
	if (!f)
		return EXIT_FAILURE;

	cgroot = strdup(cgroot1);
	/*while((ret = getline(&lbuf, &lbufsz, f)) >= 0) {*/
		/*char *type = strchr(lbuf, ':')+1;*/
		/*char *path = strchr(type, ':');*/
		/**(path++) = '\0';*/
		/*if (strlen(type) != 0)*/
			/*continue;*/
		/*rstrip(path);*/
		/*int len = snprintf(pathbuf, PATH_MAX, "%s%s", cgroot1, path);*/
		/*if (len >= PATH_MAX) {*/
			/*free(cgroot1);*/
			/*return EXIT_FAILURE;*/
		/*}*/

		/*cgroot = strdup(pathbuf);*/
		/*break;*/
	/*}*/

	/*free(cgroot1);*/
	/*if (!cgroot)*/
		/*return EXIT_FAILURE;*/

	const char *uuid = rand_string();

	int len = snprintf(pathbuf, PATH_MAX, "%s/pachrome_%s", cgroot, uuid);
	if (len >= PATH_MAX)
		return EXIT_FAILURE;

	free((void *)cgroot);
	cgroot = strdup(pathbuf);

	struct stat stbuf;
	int sret = stat(pathbuf, &stbuf);
	if (sret < 0) {
		printf("%s doesn't exist, creating...\n", pathbuf);
		sret = mkdir(pathbuf, 00755);
		if (sret < 0)
			return EXIT_FAILURE;
	} else {
		printf("%s exists, clear...\n", pathbuf);
		if (!S_ISDIR(stbuf.st_mode))
			return EXIT_FAILURE;
		if (clear_child(pathbuf) < 0)
			return EXIT_FAILURE;
	}

	len = snprintf(pathbuf, PATH_MAX, "%s/cgroup.procs", cgroot);
	if (len >= PATH_MAX)
		return EXIT_FAILURE;
	procpath = strdup(pathbuf);

#define S(x) ({ \
	int __tmp = (x); \
	if (__tmp < 0) { \
		perror(#x); \
		return EXIT_FAILURE; \
	} \
	__tmp; \
})

	cgfd = S(open(cgroot, O_RDONLY|O_DIRECTORY));

	int limit_fd = S(openat(cgfd, "memory.max", O_RDWR));
	S(write(limit_fd, argv[1], strlen(argv[1])));
	close(limit_fd);

	limit_fd = S(openat(cgfd, "memory.high", O_RDWR));
	S(write(limit_fd, argv[2], strlen(argv[2])));
	close(limit_fd);

	//int efd = memory_pressure_efd();
	//if (efd < 0)
	//	return EXIT_FAILURE;


	pid_t pid;
	if ((pid = fork()) == 0) {
		setsid();
		if (fork() == 0) {
			fclose(stdout);
			fclose(stderr);
			chdir("/");
			int ret = wait_for_cgroup_release();
			if (ret == 0)
				rmdir(cgroot);
			exit(0);
		}

		int procfd = open(procpath, O_WRONLY);
		char buf[32];
		sprintf(buf, "%d", pid);
		ret = write(procfd, buf, strlen(buf));
		if (ret < 0) {
			perror("write");
			return EXIT_FAILURE;
		}
		close(procfd);

		// Drop permission
		ret = setuid(getuid());
		if (ret != 0) {
			perror("setuid");
			return EXIT_FAILURE;
		}
		ret = setgid(getgid());
		if (ret != 0) {
			perror("setgid");
			return EXIT_FAILURE;
		}
		snprintf(pathbuf, PATH_MAX, "/tmp/stderr.%s", uuid);
		freopen(pathbuf, "w", stderr);
		snprintf(pathbuf, PATH_MAX, "/tmp/stdout.%s", uuid);
		freopen(pathbuf, "w", stdout);

		if (fork() == 0) {
			fprintf(stderr, "Run\n");
			execvp(argv[3], argv+3);
			perror("exec");
			exit(1);
		}

		// error?
		exit(0);
	}

	return EXIT_SUCCESS;

	struct ev_loop *loop = EV_DEFAULT;
	ev_signal sig_w, sig_w2;

	//ev_io_init(&pressure_w, pressure_cb, efd, EV_READ);
	ev_signal_init(&sig_w, sighandler, SIGINT);
	ev_signal_init(&sig_w2, chldhandler, SIGCHLD);

	ev_signal_start(loop, &sig_w);
	ev_signal_start(loop, &sig_w2);
	//ev_io_start(loop, &proc_w);
	//ev_io_start(loop, &pressure_w);

	ev_run(loop, 0);
}
